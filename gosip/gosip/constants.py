from django.contrib import messages


LINKEDIN_AUTH_URL = 'https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id={}&scope={}&state={}&redirect_uri={}'
LINKEDIN_AUTH_URL = 'https://www.linkedin.com/uas/oauth2/accessToken?grant_type={}&code={}&redirect_uri=YOUR_REDIRECT_URI&client_id={}&client_secret={}'
LINKEDIN_APP_SCOPE = 'r_fullprofile'
LINKEDIN_API_KEY   = '75vam3yihtk81a'
LINKEDIN_APP_STATE = 'g7WV2z9uziyPVWazd7Fv'

def retrieve_errors(request, form):
    for field in form:
        for error in field.errors:
            messages.error(request, error)
            
    if form.non_field_errors():
        for error in field.non_field_errors():
            messages.error(request, error)