# encoding: utf-8
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext

from gosip.constants import retrieve_errors
from identity.models import Respondent


def login_view(request):
    context = RequestContext(request)
    
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
    
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/history')
            else:
                messages.error(request, 'Sua conta está inativa!')
        else:
            messages.error(request, 'Login e/ou Senha Inválidos!')
    else:
        user = request.user
        
        if user.is_authenticated():
            #messages.info(request, 'Você já estava autenticado')
            return HttpResponseRedirect('/history')
    
    signup_form = UserCreationForm()
    
    return render_to_response('gosip/login.html', locals(), context)

def signup_view(request):
    context = RequestContext(request)
    
    if request.method == 'POST':
        
        signup_form = UserCreationForm(request.POST)
        
        if signup_form.is_valid():
            user = signup_form.save()
            
            respondent = Respondent()
            respondent.user = user
            respondent.show_score = (Respondent.objects.count() % 2 == 0)
            
            respondent.save()
            
            messages.success(request, 'Cadastro realizado com sucesso!')
            
            new_user = authenticate(username=request.POST['username'],
                                    password=request.POST['password1'])
            login(request, new_user)
            
            return HttpResponseRedirect(reverse('history.views.step'))
        else:
            retrieve_errors(request, signup_form)
    else:
        signup_form = UserCreationForm()
        
    return render_to_response('gosip/login.html', locals(), context)

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')

def about(request):
    return render(request, 'gosip/about.html')