'''
Created on 28/05/2014

@author: Welington
'''
from django.contrib import admin

from identity.models import Respondent


admin.site.register(Respondent)