from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'gosip.views.login_view'),
    url(r'^login/$', 'gosip.views.login_view', name='login'),
    url(r'^logout/$', 'gosip.views.logout_view', name='logout'),
    url(r'^signup/$', 'gosip.views.signup_view', name='signup'),
    url(r'^about/$', 'gosip.views.about'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^history/', include('history.urls')),
    url(r'^identity/', include('identity.urls')),
    url(r'^map/', include('map.urls', namespace='map')),
    url(r'^social-profile/', include('social_profile.urls', namespace='social_profile')),
    #(r'^inplaceeditform/', include('inplaceeditform.urls')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),    
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'history.views.not_found'