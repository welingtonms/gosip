# coding: utf-8
'''
Created on 08/07/2014

@author: Welington
'''
from django.contrib.auth.forms import UserCreationForm


class GosipUserCreationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(GosipUserCreationForm, self).__init__(*args, **kwargs)

        # if you want to do it to all of them
        for field in self.fields.values():
            print field
            field.error_messages = {'required':'O campo {fieldname} é obrigatório'.format(
                fieldname=field.label)}