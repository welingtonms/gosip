import json
import urllib2

FRIEND_LIMIT = 10
NAME_KEY    = 'name'
EMAIL_KEY   = 'email'
PHOTO_KEY   = 'photo'

class ProviderOperations():
    
    def __init__(self, request):
        self.request = request
        
    def get_friends(self):
        friends = []
    
        request = urllib2.Request(self._get_friend_url())
        friends_dic = json.loads(urllib2.urlopen(request).read()).get(self.friend_tag)
        for f in friends_dic:
            friends.append(self._get_friendname(f))
                
        return friends        
    
    def get_user_profile(self):
        request     = urllib2.Request(self._get_user_profile_url())
        user_json   = json.loads(urllib2.urlopen(request).read())
        return self._get_user_profile(user_json)        
    
    def _get_social_auth(self):
        return self.request.user.social_auth.get(provider=self.provider_name)
    
    def _get_friendname(self, friend_json):
        raise NotImplementedError
    
    def _get_friend_url(self):
        raise NotImplementedError
    
    def _get_user_profile_url(self):
        raise NotImplementedError
    
    def _get_user_profile(self, user_json):
        raise NotImplementedError
    
class FacebookProviderOperations(ProviderOperations):
    provider_name   = 'facebook'
    friend_tag      = 'data'
        
    def _get_friendname(self, friend_json):
        return friend_json['name'].encode("utf-8")
    
    def _get_friend_url(self):
        social_auth = self._get_social_auth()
        return "https://graph.facebook.com/me/friends?limit={}&access_token={}".format(FRIEND_LIMIT, social_auth.extra_data['access_token'])
    
    def _get_user_profile_url(self):
        social_auth = self._get_social_auth()
        return "https://graph.facebook.com/me?access_token={}".format(social_auth.extra_data['access_token'])
    
    def _get_user_profile(self, user_json):
        name = user_json['name']
        email = user_json['email']
        photo = "http://graph.facebook.com/{}/picture".format(user_json['id'])        
        return {NAME_KEY : name, EMAIL_KEY: email, PHOTO_KEY: photo}

class LinkedinProviderOperations(ProviderOperations):
    provider_name   = 'linkedin-oauth2'
    friend_tag      = 'values'
    
    def _get_friendname(self, friend_json):
        return "{} {}".format(friend_json['firstName'].encode("utf-8"), friend_json['lastName'].encode("utf-8"))
    
    def _get_friend_url(self):
        social_auth = self._get_social_auth()
        return "https://api.linkedin.com/v1/people/~/connections?count={}&oauth2_access_token={}&format=json".format(FRIEND_LIMIT, social_auth.extra_data['access_token'])
    
    def _get_user_profile_url(self):
        fields = '(id,first-name,last-name,industry,location,email-address,date-of-birth,public-profile-url,picture-url)'
        social_auth = self._get_social_auth()
        return "https://api.linkedin.com/v1/people/~:{}?oauth2_access_token={}&format=json&".format(fields, social_auth.extra_data['access_token'])
    
    def _get_user_profile(self, user_json):
        name = "{} {}".format(user_json['firstName'], user_json['lastName'])
        email = user_json['emailAddress']
        photo = user_json['pictureUrl']
        return {NAME_KEY : name, EMAIL_KEY: email, PHOTO_KEY: photo}       
