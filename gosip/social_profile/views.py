from django.http.response import HttpResponseRedirect

from history.models import UserStep
from history.views import retrieve_step
from identity.models import Respondent
from social_profile.operations import FacebookProviderOperations, LinkedinProviderOperations, \
    NAME_KEY, EMAIL_KEY, PHOTO_KEY


PROVIDER_SESSION_KEY = 'social_auth_last_login_backend'
FACEBOOK_PROVIDER    = 'facebook'
LINKEDIN_PROVIDER    = 'linkedin-oauth2'

def get_user_data(request):
    if request.user.is_authenticated():
        providers = request.user.social_auth.all()
        
        if providers.count() <= 0:
            return HttpResponseRedirect('/history')
        
        provider = providers.latest('id').provider            
            
        if provider == FACEBOOK_PROVIDER:
            has_linkedin = False
            has_facebook = True
            provider_operations = FacebookProviderOperations(request=request)            
        elif provider == LINKEDIN_PROVIDER:
            has_linkedin = True
            has_facebook = False
            provider_operations = LinkedinProviderOperations(request=request)            
        else:
            return HttpResponseRedirect('/history')
        
        user_profile    = provider_operations.get_user_profile()
        #friends         = provider_operations.get_friends()
        
        respondent = save_respondent(request.user, user_profile, has_linkedin, has_facebook)
        
        user_step      = retrieve_step(respondent)
        user_step.step = '2c' 
        user_step.save()
                
        #save_friends(respondent, friends)
        
    return HttpResponseRedirect('/history')

def save_respondent(user, user_profile, has_linkedin, has_facebook):
    name         = user_profile[NAME_KEY]
    email        = user_profile[EMAIL_KEY]
    photo        = user_profile[PHOTO_KEY]
    
    respondent, _ = Respondent.objects.get_or_create(user=user)
    respondent.name=name
    respondent.email=email
    respondent.photo=photo
    respondent.has_linkedin=has_linkedin
    respondent.has_facebook=has_facebook
    respondent.save()
    
    return respondent

def save_friends(respondent, friends):
    pass
    #===========================================================================
    # friends_obj = []
    # 
    # for f in friends:
    #     friend = Friend(name=f)
    #     friend.save()
    #     friends_obj.append(friend)
    #     
    # respondent.friends = friends_obj
    # respondent.save()
    #===========================================================================