# coding: utf-8

from django import forms
from django.forms.widgets import RadioFieldRenderer, RadioInput
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe

from identity.constants import IPI_SECRET, IPI_CLOSED, IPI_PUBLIC, MODE_CONTROL, \
    MODE_CONVENIENCE, EXPOSURE_ASK, EXPOSURE_ALLOW, EXPOSURE_DENY
from identity.models import Respondent

class RespondentForm(forms.ModelForm):

    class Meta:
        model = Respondent

class FriendForm(forms.Form):
    name       = forms.CharField(max_length=200, min_length=1, error_messages={'required':'jhksjdhskjh'}) 
        
IPI_CHOICES = (
    ('1', IPI_SECRET),
    ('2', IPI_CLOSED),
    ('3', IPI_PUBLIC),
) 

USAGE_MODE = (
    ('1', MODE_CONTROL),
    ('2', MODE_CONVENIENCE),
)

EXPOSURE_MODE = (
    ('1', EXPOSURE_ASK),
    ('2', EXPOSURE_ALLOW),
    ('3', EXPOSURE_DENY),
)

GENDER_CHOICES = (
    ('1','Masculino'),
    ('2','Feminino'),
)

class GosipRadioInput(RadioInput):
    def __unicode__(self):
        #return mark_safe(u'<input type="radio" name="%s" id="id_%s_%s" value="%s"> %s' % (self.name, self.name, self.index, self.value, self.choice_label,))
        return mark_safe(u'%s %s' % (self.tag(),self.choice_label,))
            
class GosipRadioRenderer(RadioFieldRenderer):
    
    def render(self):
        return mark_safe(u''.join([u'<label class="checkbox-inline">%s</label>' % force_unicode(w) for w in self]))
    
    def __iter__(self):
        for i, choice in enumerate(self.choices):
            yield GosipRadioInput(self.name, self.value, self.attrs.copy(), choice, i)

class ManageableData(forms.Form):
    ipi      = forms.IntegerField(max_value = 100, min_value = 0)
    #mode     = forms.ChoiceField(choices=USAGE_MODE, widget=forms.RadioSelect(renderer=GosipRadioRenderer), initial=1)
    mode     = forms.ChoiceField(choices=USAGE_MODE, widget=forms.HiddenInput(), initial=1)
    
    exposure = forms.ChoiceField(choices=EXPOSURE_MODE, widget=forms.HiddenInput(), initial=1)

class PersonalData(ManageableData):
    name     = forms.CharField(max_length=150, min_length=2)
    email    = forms.CharField(max_length=150, min_length=2)

class FinancialData(ManageableData):
    value = forms.CharField(max_length=1, min_length=1, required=False)

class LocationData(ManageableData):
    value = forms.CharField(max_length=1, min_length=1, required=False)

class ConsumptionData(ManageableData):
    value = forms.CharField(max_length=1, min_length=1, required=False)
        
class MedicalData(ManageableData):
    birth_date       = forms.DateField(widget=forms.DateInput(format='%d/%m/%Y'), required=False)
    gender           = forms.ChoiceField(choices=GENDER_CHOICES, required=False) 

class RelationshipData(ManageableData):
    value = forms.CharField(max_length=1, min_length=1)
