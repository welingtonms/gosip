# coding: utf-8

# Create your views here.
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http.response import HttpResponse

from identity.constants import SETTINGS_PERSONAL, SETTINGS_FINANCIAL, \
    SETTINGS_LOCATION, SETTINGS_RELATION, SETTINGS_CONSUMPTION, SETTINGS_MEDICAL
from identity.forms import PersonalData, FinancialData, MedicalData, FriendForm, \
    ConsumptionData
from identity.models import Respondent, PrivacySettings
from django.core.exceptions import ObjectDoesNotExist


@login_required
def save_personal_settings(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        personal_data = PersonalData(request.POST)
        
        if personal_data.is_valid():
            data  = personal_data.cleaned_data
            
            name     = data['name']
            email    = data['email']
            ipi      = data['ipi']
            mode     = data['mode']
            exposure = data['exposure']
            
            respondent.name  = name
            respondent.email = email
            respondent.save()
            
            #privacy_settings,_ = PrivacySettings.objects.get_or_create(respondent = respondent, type = SETTINGS_PERSONAL)
            privacy_settings            = PrivacySettings()
            privacy_settings.type       = SETTINGS_PERSONAL
            privacy_settings.respondent = respondent
            privacy_settings.ipi        = ipi
            privacy_settings.mode       = mode
            privacy_settings.exposition = exposure
            privacy_settings.save()
    
            messages.success(request, 'Dados pessoais e respectivas configurações salvos com sucesso!')        
            return HttpResponse(status = 200)
        else:
            messages.error(request, 'Erro no preenchimento do formulário de dados pessoais')
                
    return HttpResponse(status = 400)

@login_required
def save_financial_settings(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        financial_data = FinancialData(request.POST)
        
        if financial_data.is_valid():
            data  = financial_data.cleaned_data
    
            ipi      = data['ipi']
            mode     = data['mode']
            exposure = data['exposure']
            
            #privacy_settings,_ = PrivacySettings.objects.get_or_create(respondent = respondent, type = SETTINGS_FINANCIAL)
            privacy_settings            = PrivacySettings()
            privacy_settings.type       = SETTINGS_FINANCIAL
            privacy_settings.respondent = respondent
            privacy_settings.ipi        = ipi
            privacy_settings.mode       = mode
            privacy_settings.exposition = exposure
            privacy_settings.save()
    
            messages.success(request, 'Dados financeiros e respectivas configurações salvos com sucesso!')        
            return HttpResponse(status = 200)
        else:
            print financial_data.errors
            messages.error(request, 'Erro no preenchimento do formulário de dados financeiros')

    return HttpResponse(status = 400)

@login_required
def save_consumption_settings(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        consumption_data = ConsumptionData(request.POST)
        
        if consumption_data.is_valid():
            data  = consumption_data.cleaned_data
    
            ipi      = data['ipi']
            mode     = data['mode']
            exposure = data['exposure']
            
            #privacy_settings,_ = PrivacySettings.objects.get_or_create(respondent = respondent, type = SETTINGS_CONSUMPTION)
            privacy_settings            = PrivacySettings()
            privacy_settings.type       = SETTINGS_CONSUMPTION
            privacy_settings.respondent = respondent
            privacy_settings.ipi        = ipi
            privacy_settings.mode       = mode
            privacy_settings.exposition = exposure
            privacy_settings.save()
    
            messages.success(request, 'Dados de Consumo e respectivas configurações salvos com sucesso!')        
            return HttpResponse(status = 200)
        else:
            print consumption_data.errors
            messages.error(request, 'Erro no preenchimento do formulário de dados de consumo')

    return HttpResponse(status = 400)

@login_required
def save_location_settings(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        location_data = FinancialData(request.POST)
        
        if location_data.is_valid():
            data  = location_data.cleaned_data
    
            ipi      = data['ipi']
            mode     = data['mode']
            exposure = data['exposure']
            
            #privacy_settings,_ = PrivacySettings.objects.get_or_create(respondent = respondent, type = SETTINGS_LOCATION)
            privacy_settings            = PrivacySettings()
            privacy_settings.type       = SETTINGS_LOCATION
            privacy_settings.respondent = respondent
            privacy_settings.ipi        = ipi
            privacy_settings.mode       = mode
            privacy_settings.exposition = exposure
            privacy_settings.save()
    
            messages.success(request, 'Dados de localização e respectivas configurações salvos com sucesso!')        
            return HttpResponse(status = 200)
        else:
            messages.error(request, 'Erro no preenchimento do formulário de dados de localização')

    return HttpResponse(status = 400)

@login_required
def save_friend(request):
    user = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        friend_form = FriendForm(request.POST)
        
        if friend_form.is_valid():
            data  = friend_form.cleaned_data
            name  = data['name']
            
            try:
                user_friend = User.objects.get(username = name)
                
                friend = Respondent.objects.get(user = user_friend)
                if friend and friend.pk != respondent.pk:
                    respondent.friends.add(friend)
        
                    messages.success(request, 'Novo amigo salvo com sucesso!')        
                    return HttpResponse(status = 200)
                else:
                    messages.error(request, 'Você não pode se adicionar como amigo!')
            except ObjectDoesNotExist:
                messages.error(request, 'Não foi encontrado um usuário com esse login!')
        else:
            messages.error(request, 'Erro no preenchimento do formulário de dados de relacionamento')

    return HttpResponse(status = 400)

@login_required
def save_relationship_settings(request):
    user = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        relationship_data = FinancialData(request.POST)
        
        if relationship_data.is_valid():
            data  = relationship_data.cleaned_data
    
            ipi      = data['ipi']
            mode     = data['mode']
            exposure = data['exposure']
            
            #privacy_settings,_ = PrivacySettings.objects.get_or_create(respondent = respondent, type = SETTINGS_RELATION)
            privacy_settings            = PrivacySettings()
            privacy_settings.type       = SETTINGS_RELATION
            privacy_settings.respondent = respondent
            privacy_settings.ipi        = ipi
            privacy_settings.mode       = mode
            privacy_settings.exposition = exposure
            privacy_settings.save()
    
            messages.success(request, 'Dados de relacionamento e respectivas configurações salvos com sucesso!')        
            return HttpResponse(status = 200)
        else:
            messages.error(request, 'Erro no preenchimento do formulário de dados de relacionamento')

    return HttpResponse(status = 400)

@login_required
def save_medical_settings(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        medical_data = MedicalData(request.POST)
        
        if medical_data.is_valid():
            data  = medical_data.cleaned_data
    
            birth_date       = data['birth_date']
            gender           = data['gender']
    
            respondent.birth_date = birth_date
            respondent.gender     = gender
            respondent.save()
    
            ipi      = data['ipi']
            mode     = data['mode']
            exposure = data['exposure']
            
            #privacy_settings,_ = PrivacySettings.objects.get_or_create(respondent = respondent, type = SETTINGS_FINANCIAL)
            privacy_settings            = PrivacySettings()
            privacy_settings.type       = SETTINGS_MEDICAL
            privacy_settings.respondent = respondent
            privacy_settings.ipi        = ipi
            privacy_settings.mode       = mode
            privacy_settings.exposition = exposure
            privacy_settings.save()
    
            messages.success(request, 'Dados médicos e respectivas configurações salvos com sucesso!')        
            return HttpResponse(status = 200)
        else:
            print medical_data.errors
            messages.error(request, 'Erro no preenchimento do formulário de dados médicos')

    return HttpResponse(status = 400)
