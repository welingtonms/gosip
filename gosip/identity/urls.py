from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^save/personal/$', 'identity.views.save_personal_settings'),
    url(r'^save/financial/$', 'identity.views.save_financial_settings'),
    url(r'^save/location/$', 'identity.views.save_location_settings'),
    url(r'^save/relationship/$', 'identity.views.save_relationship_settings'),
    url(r'^save/medical/$', 'identity.views.save_medical_settings'),
    url(r'^save/friend/$', 'identity.views.save_friend'),
    url(r'^save/consumption/$', 'identity.views.save_consumption_settings'),
    #url(r'^linkedin/auth/$', views.get_linkedin_auth_token, name='linkedin-auth'),
    #url(r'^linkedin/access/$', views.get_linkedin_access_token, name='linkedin-access'),
)
