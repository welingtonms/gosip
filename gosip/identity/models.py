#coding: utf-8

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

class Respondent(models.Model):
    user         = models.OneToOneField(User)
    has_linkedin = models.BooleanField(default=False)
    has_facebook = models.BooleanField(default=False)

    installed_fakebook = models.BooleanField(default=False)
    installed_twistter = models.BooleanField(default=False)
    installed_gugou    = models.BooleanField(default=False)

    installed_health_monitor  = models.BooleanField(default=False)
    installed_hospital_assist = models.BooleanField(default=False)
    installed_smart_meter     = models.BooleanField(default=False)

    took_photo    = models.BooleanField(default=False)
    bought_car    = models.BooleanField(default=False)
    called_to     = models.CharField(max_length = 20, blank=True, null=True )
    used_twistter = models.BooleanField(default=False)

    show_score = models.BooleanField(default=True)

    name         = models.CharField(max_length = 200, null=True, blank=True)
    email        = models.CharField(max_length = 200, null=True, blank=True)
    photo        = models.CharField(max_length = 200, null=True, blank=True)
    birth_date   = models.DateField(null=True, blank=True)
    gender       = models.CharField(max_length=20, null=True, blank=True)

    navigation_trial = models.IntegerField(default=11)

    friends = models.ManyToManyField("self")
    
    def __unicode__(self):
        if self.name:
            return self.name + ' ('+ self.user.username + ')'
        
        return self.user.username

class PrivacySettings(models.Model):
    respondent = models.ForeignKey(Respondent)
    ipi        = models.IntegerField(default=100)
    mode       = models.CharField(max_length=20, default='1')
    exposition = models.CharField(max_length=20, default='1')
    type       = models.CharField(max_length=50)
    modified   = models.DateTimeField(default=timezone.now())

    class Meta:
        ordering = ['-id']