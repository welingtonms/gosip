# coding: utf-8

import random

from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext

from history.models import UserStep, Score, Message
from history.views import retrieve_step
from identity.models import Respondent
from map.constants import NAV_SETTINGS_KEY, MAP_SETTINGS, \
    SOURCE_INDEX, TARGET_INDEX, SUCCESS_INDEX, IMAGE_MAP_INDEX, \
    BURGUERKEEN_TO_CASA, FLOODED_EDGES_INDEX, FAIL_FLOODED_INDEX, GOAL_INDEX,\
    FAIL_TIME_INDEX
from map.models import Edge, Place


def navigation(request, next=None):
    respondent = get_respondent(request)
    score,_    = Score.objects.get_or_create(respondent = respondent)
    inbox      = Message.objects.filter(receiver = respondent)
    inbox_unread = inbox.filter(read = False).count() 
    
    if next == None: # is source
        next = get_source(request.session[NAV_SETTINGS_KEY])
    else:
        next = Edge.objects.get(pk=next)             
    
    #verifica se entrou em rua alagada    
    if request.session[NAV_SETTINGS_KEY] == BURGUERKEEN_TO_CASA and is_flooded_edge(next, BURGUERKEEN_TO_CASA):
        set_next_step(request, FAIL_FLOODED_INDEX)
        return HttpResponseRedirect(reverse('history.views.step'))  
    
    if request.session[NAV_SETTINGS_KEY] == BURGUERKEEN_TO_CASA and respondent.navigation_trial < 1:
        set_next_step(request, FAIL_TIME_INDEX)
        return HttpResponseRedirect(reverse('history.views.step'))
    
    #verifica se o usuário pode receber notificações com dicas de transito
    if request.session[NAV_SETTINGS_KEY] == BURGUERKEEN_TO_CASA and respondent.used_twistter:
        floodeds_pk = MAP_SETTINGS[BURGUERKEEN_TO_CASA][FLOODED_EDGES_INDEX]
        edge = Edge.objects.get(pk = random.choice(floodeds_pk))
        traffic_tip = 'Alagamento na ' + str(edge.label) 
        
    if respondent.installed_gugou:
        image_map = get_image_map(request.session[NAV_SETTINGS_KEY])
    
    if request.session[NAV_SETTINGS_KEY] == BURGUERKEEN_TO_CASA:    
        respondent.navigation_trial -= 1
        respondent.save()
    
    
    goal = get_goal(request.session[NAV_SETTINGS_KEY])
             
    return render_to_response('map/navigation.html', locals() , context_instance=RequestContext(request))

def place(request, previous_edge, place_pk):
    previous_edge   = Edge.objects.get(pk=previous_edge)
    place           = Place.objects.get(pk=place_pk)
    
    if is_target(place, request.session[NAV_SETTINGS_KEY]):
        set_next_step(request, SUCCESS_INDEX)
        return HttpResponseRedirect(reverse('history.views.step'))        
    
    #TODO Quando acontecer alguma coisa errada
    #set_next_step(request, WRONG_INDEX)
    #return HttpResponseRedirect(reverse('history.views.step'))
    
    return render_to_response('map/place.html', locals() , context_instance=RequestContext(request))

def get_respondent(request):
    return Respondent.objects.get(user = request.user)
    
def get_source(nav_settings_key):
    source_pk = MAP_SETTINGS[nav_settings_key][SOURCE_INDEX] 
    return Edge.objects.get(pk=source_pk) 

def is_target(place, nav_settings_key):
    targets = MAP_SETTINGS[nav_settings_key][TARGET_INDEX]
    return place.pk in targets

def set_next_step(request, type_index):
    respondent = Respondent.objects.get(user = request.user)
    next_step  = get_next_step(request.session[NAV_SETTINGS_KEY], type_index)
    
    user_step = retrieve_step(respondent)
    
    if not user_step.answer:
        user_step.answer = next_step
        user_step.save()
        
        user_step = UserStep()
        user_step.respondent = respondent
        user_step.step       = next_step
        user_step.save()
        
    #UserStep.objects.filter(respondent = respondent).update(step = next_step)
    
def get_next_step(nav_settings_key, type_index):
    return MAP_SETTINGS[nav_settings_key][type_index]

def get_image_map(nav_settings_key):
    return MAP_SETTINGS[nav_settings_key][IMAGE_MAP_INDEX]

def get_goal(nav_settings_key):
    return MAP_SETTINGS[nav_settings_key][GOAL_INDEX]
    
def is_flooded_edge(edge, nav_settings_key):
    floodeds_pk = MAP_SETTINGS[nav_settings_key][FLOODED_EDGES_INDEX]
    return edge.pk in floodeds_pk