from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'map.views.navigation'),
    url(r'^(?P<next>\d+)$', 'map.views.navigation', name='navigation'),
    url(r'^place/(?P<previous_edge>\d+)/(?P<place_pk>\d+)$', 'map.views.place', name='place'),           
)
