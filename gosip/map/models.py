# coding=utf-8
'''
Created on 07/06/2014

@author: Welington
'''

from django.db import models

from map.constants import ENTRADA_PK, BANCA_PK, PET_SHOP_PK, VIKINGS_PK, \
    SUPERMERCADO_PK, ELETRIC_PK, ACOUGUE_PK, ACADEMIA_PK, BURGUER_KEEN_PK, \
    LOTERICA_PK, ESCOLA_PK, PADARIA_PK, PARQUE_PK, CLUBE_PK, CASA_PK, \
    HOSPITAL_PK, SHOPPING_PK, MUSEU_PK, POLICIA_PK, CONCESSIONARIA_PK,\
    GOVERNO_PK, PRACA_PK, BOMBEIROS_PK


class Place(models.Model):
    label           = models.CharField(max_length = 200)
    
class Edge(models.Model):
    label           = models.CharField(max_length = 100)
    possibilities   = models.ManyToManyField('self', related_name="possibilities", null=True, blank=True)
    places          = models.ManyToManyField(Place, related_name="places", null=True, blank=True)
    
    def set_possibilities(self, possibilities):
        self.possibilities = possibilities
        self.save()
    
    def set_places(self, places):     
        self.places = places
        self.save()

def update_edge(edge, possibilities, places):
    edge.set_possibilities(possibilities)
    edge.set_places(places)
    edge.save()    
        
def load_map():
    entrada          = Place.objects.create(pk=ENTRADA_PK, label="Entrada da Cidade")
    banca            = Place.objects.create(pk=BANCA_PK, label="Banca de Jornal")
    pet_shop         = Place.objects.create(pk=PET_SHOP_PK, label="Pet Shop")
    vikings_bar      = Place.objects.create(pk=VIKINGS_PK, label="Viking's Bar")
    supermercado     = Place.objects.create(pk=SUPERMERCADO_PK, label="Supermercado")
    eletric_privus   = Place.objects.create(pk=ELETRIC_PK, label="Electric Privus")
    acougue          = Place.objects.create(pk=ACOUGUE_PK, label="Açougue")
    academia         = Place.objects.create(pk=ACADEMIA_PK, label="Academia")
    burguer_keen     = Place.objects.create(pk=BURGUER_KEEN_PK, label="Burguer Queen")
    loterica         = Place.objects.create(pk=LOTERICA_PK, label="Lotérica")
    escola           = Place.objects.create(pk=ESCOLA_PK, label="Escola")
    padaria          = Place.objects.create(pk=PADARIA_PK, label="Padaria")
    parque           = Place.objects.create(pk=PARQUE_PK, label="Parque Azul")
    clube            = Place.objects.create(pk=CLUBE_PK, label="Clube")
    casa             = Place.objects.create(pk=CASA_PK, label="Sua Casa")
    
    hospital         = Place.objects.create(pk=HOSPITAL_PK, label="Hospital Saint Joseph")
    shopping         = Place.objects.create(pk=SHOPPING_PK, label="Maximus Shopping")
    museu            = Place.objects.create(pk=MUSEU_PK, label="Museu Histórico de Privus")
    policia          = Place.objects.create(pk=POLICIA_PK, label="Centro de Comando Policial")
    concessionaria   = Place.objects.create(pk=CONCESSIONARIA_PK , label="Concessionária Auto Privus")
    governo          = Place.objects.create(pk=GOVERNO_PK, label="Casa do Governo Privense")
    praca            = Place.objects.create(pk=PRACA_PK, label="Praça Central da Catedral")
    bombeiro         = Place.objects.create(pk=BOMBEIROS_PK, label="Corpo de Bombeiros")
    
    e1 = Edge.objects.create(label="E1 - Av. Governador dos Palmares")
    e2 = Edge.objects.create(label="E2 - Av. Governador dos Palmares")
    e3 = Edge.objects.create(label="E3 - Av. Governador dos Palmares")
    e4 = Edge.objects.create(label="E4 - Av. Governador dos Palmares")
    
    e5 = Edge.objects.create(label="E5 - Rua 7 de Agosto")
    e6 = Edge.objects.create(label="E6 - Rua 7 de Agosto")
    e7 = Edge.objects.create(label="E7 - Rua 7 de Agosto")
    e8 = Edge.objects.create(label="E8 - Rua 7 de Agosto")
    
    e9 = Edge.objects.create(label="E9 - Rua Armando Beldi")
    e10 = Edge.objects.create(label="E10 - Rua Armando Beldi")
    e11 = Edge.objects.create(label="E11 - Rua Armando Beldi")
    e12 = Edge.objects.create(label="E12 - Rua Armando Beldi")
    
    e13 = Edge.objects.create(label="E13 - Rua da Inovação")
    e14 = Edge.objects.create(label="E14 - Rua da Inovação")
    e15 = Edge.objects.create(label="E15 - Rua da Inovação")
    e16 = Edge.objects.create(label="E16 - Rua da Inovação")
    
    e17 = Edge.objects.create(label="E17 - Rua André Américo")
    e18 = Edge.objects.create(label="E18 - Rua André Américo")
    e19 = Edge.objects.create(label="E19 - Rua André Américo")
    e20 = Edge.objects.create(label="E20 - Rua André Américo")
    
    e21 = Edge.objects.create(label="E21 - Rua Joaquim Albuquerque")
    e22 = Edge.objects.create(label="E22 - Rua Joaquim Albuquerque")
    e23 = Edge.objects.create(label="E23 - Rua Joaquim Albuquerque")
    e24 = Edge.objects.create(label="E24 - Rua Joaquim Albuquerque")
    
    update_edge(edge=e1, possibilities=[e13, e2, e14], places=[entrada, banca])
    update_edge(edge=e2, possibilities=[e1, e13, e14, e17, e3, e18], places=[pet_shop, hospital])
    update_edge(edge=e3, possibilities=[e2, e17, e18, e21, e4, e22], places=[vikings_bar, governo])
    update_edge(edge=e4, possibilities=[e3, e21, e22], places=[supermercado, eletric_privus])
    
    update_edge(edge=e5, possibilities=[e14, e6, e15], places=[policia, bombeiro])
    update_edge(edge=e6, possibilities=[e5, e14, e15, e18, e7, e19], places=[acougue, academia])
    update_edge(edge=e7, possibilities=[e6, e18, e19, e22, e8, e23], places=[burguer_keen, praca])
    update_edge(edge=e8, possibilities=[e7, e22, e23], places=[loterica, shopping])
    
    update_edge(edge=e9,  possibilities=[e15, e10, e16], places=[escola, parque])
    update_edge(edge=e10, possibilities=[e9, e15, e16, e19, e11, e20], places=[museu])
    update_edge(edge=e11, possibilities=[e10, e19, e20, e23, e12, e24], places=[clube, concessionaria])
    update_edge(edge=e12, possibilities=[e11, e23, e24], places=[padaria, casa])
    
    update_edge(edge=e13, possibilities=[e1, e2, e14], places=[])
    update_edge(edge=e14, possibilities=[e1, e2, e5, e6, e13, e15], places=[])
    update_edge(edge=e15, possibilities=[e5, e6, e9, e10, e14, e16], places=[])
    update_edge(edge=e16, possibilities=[e9, e10, e15], places=[])
    
    update_edge(edge=e17, possibilities=[e2, e3, e18], places=[])
    update_edge(edge=e18, possibilities=[e2, e3, e6, e7, e17, e19], places=[])
    update_edge(edge=e19, possibilities=[e6, e7, e10, e11, e18, e20], places=[])
    update_edge(edge=e20, possibilities=[e10, e11, e19], places=[])
    
    update_edge(edge=e21, possibilities=[e3, e4, e22], places=[])
    update_edge(edge=e22, possibilities=[e3, e4, e7, e8, e21, e23], places=[])
    update_edge(edge=e23, possibilities=[e7, e8, e11, e12, e22, e14], places=[])
    update_edge(edge=e24, possibilities=[e11, e12, e23], places=[])  