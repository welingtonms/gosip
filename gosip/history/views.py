# coding: utf-8

# Create your views here.
from __future__ import division
import decimal

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.context_processors import request
from django.core.urlresolvers import reverse
from django.db.models.query_utils import Q
from django.http.response import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from django.utils import timezone

from history.forms import StepForm, DeductForm, ServiceForm, CallForm, \
    TwistterForm, FakebookForm
from history.models import UserStep, STEP, Score, UserChoice, CHOICE, Step, \
    Message
from identity.constants import SETTINGS_PERSONAL, SETTINGS_FINANCIAL, \
    SETTINGS_LOCATION, SETTINGS_MEDICAL, SETTINGS_RELATION, SETTINGS_CONSUMPTION
from identity.forms import PersonalData, FinancialData, LocationData, \
    MedicalData, FriendForm, RelationshipData, ConsumptionData
from identity.models import Respondent, PrivacySettings
from map.constants import NAV_SETTINGS_KEY, SUPERMERCADO_TO_ENTRADA, \
    CASA_TO_PAGARCONTA, BURGUERKEEN_TO_CASA

def get_template_path(page):
    return 'history/{}.html'.format(page)
    
def retrieve_respondent_and_step(user = None):
    if user is None:
        return None, None
    
    respondent = Respondent.objects.get(user = user)
    user_step  = retrieve_step(respondent)

    step       = STEP[user_step.step]
    
    if step is None:
        raise Http404
    
    return respondent, step

@login_required
def home(request):    
    return step(request)
 
def profile(request):
    user                  = request.user
    respondent, prev_step = retrieve_respondent_and_step(user)
    score,_               = Score.objects.get_or_create(respondent = respondent)
    friends               = respondent.friends.all()

    data = add_utility_forms('2c', locals(), respondent, request)  
    
    return render(request, get_template_path('profile') , data)
    
@login_required
def step(request):
    user                  = request.user
    respondent, prev_step = retrieve_respondent_and_step(user)
    score,_               = Score.objects.get_or_create(respondent = respondent)
    
    inbox  = Message.objects.filter(receiver = respondent)
    inbox_unread = inbox.filter(read = False).count() 
    friends   = respondent.friends.all()
        
    if friends:
        first_friend = friends[0]
    else:
        first_friend = None

    if request.method == 'POST':
        step_form = StepForm(request.POST)
        
        if step_form.is_valid():
            data  = step_form.cleaned_data
            
            step_page   = data['step_page']
            step_answer = data['step_answer']
            
            step_page = step_page.lower()
            curr_step = STEP[step_page]
            
            if curr_step is None:
                print 'curr_step nao pode ser None' # raise Http404
                messages.error(request, 'Curr_step e None')
            else:
                step_answer = step_answer.lower()
                step_page   = step_page.lower()
                                
                goes_to     = curr_step.get_next(step_answer)
                step        = STEP[goes_to]
                
                user_step   = retrieve_step(respondent)
                if is_repeated(respondent, step_page):
                    messages.error(request, 'Você não pode retroceder na história!')
                else:
                    #atualizando user step
                    user_step.answer = step_answer
                    user_step.save()
                    
                    for choice_item in CHOICE[step_page]:
                        choice = UserChoice()
                        choice.respondent     = respondent
                        choice.data_type      = choice_item.data_type
                        choice.context        = step_page
                        choice.provider       = choice_item.provider
                        choice.status         = choice_item.status
                        choice.exposure_index = calculate_exposure(respondent, choice.data_type)
                        choice.save()
                    
                    score.risk       += step.risk_impact
                    if step.page == '7d':
                        if step_page == '6c':
                            score.money      -= (score.money * decimal.Decimal(0.765))
                        elif step_page == '6d':
                            score.money      -= (score.money *decimal.Decimal( 0.9))
                    else:
                        score.money      -= decimal.Decimal(step.money_impact)
                    score.influence  += step.influence_impact
                    score.save()
                    
                    #criando novo user step                
                    user_step = UserStep()
                    user_step.respondent = respondent
                    user_step.step = step.page
                    user_step.save()
                    
                    prev_step = step

        else:
            messages.error(request, 'Erro no preenchimento do formulario')
    
    curr_page    = prev_step.page
    step_form    = StepForm()
    service_form = ServiceForm()

    if curr_page == '6a' or curr_page == '10b' or curr_page == '10c':
        return HttpResponseRedirect(reverse('map:map.views.navigation'))    
            
    data = add_utility_forms(curr_page, locals(), respondent, request)    
    
    return render(request, get_template_path(curr_page), data)

@login_required
def deduct_value(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        deduct_form = DeductForm(request.POST)
        
        if deduct_form.is_valid():
            data  = deduct_form.cleaned_data
            value = data['value']
            context = data['context']
            
            score,_ = Score.objects.get_or_create(respondent = respondent)
            score.money     -= decimal.Decimal(value)
            score.risk      += 5
            score.influence += 10
            score.save()
            
            choice = UserChoice()
            choice.respondent     = respondent
            choice.data_type      = 'image'
            choice.context        = context
            choice.provider       = 'burger queen'
            choice.status         = ''
            choice.exposure_index = 0.0
            choice.save()
            
            messages.success(request, 'Checkin feito com sucesso. Escolha se vai pagar em Cartão ou em Dinheiro!')
                        
            return HttpResponse(status = 200)
        else:
            return HttpResponse(status = 400)

def install_service(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    score,_    = Score.objects.get_or_create(respondent = respondent)

    if request.method == 'POST':
        service_form = ServiceForm(request.POST)
        
        if service_form.is_valid():
            data  = service_form.cleaned_data
            
            service  = data['service']
            exposure = data['exposure']
            
            choice = UserChoice()
            choice.respondent     = respondent
            choice.data_type      = exposure
            choice.context        = 'install-' + service
            choice.provider       = ''
            choice.status         = ''
            choice.exposure_index = 0.0
            choice.save()
            
            if service == 'fakebook':
                respondent.installed_fakebook = True
                score.risk      += 10 # pessoais e relacion
                score.money     -= decimal.Decimal(0)
                score.influence += 10
                score.save()
                
            elif service == 'smart-meter':
                respondent.installed_smart_meter = True
                score.risk      += 5 # consumo
                score.money     -= decimal.Decimal(2)
                score.influence += 0
                score.save()
                
            elif service == 'location':
                respondent.installed_gugou = True
                score.risk      += 5 # localização
                score.money     -= decimal.Decimal(0)
                score.influence += 0
                score.save()
                
            elif service == 'health-monitor':
                respondent.installed_health_monitor = True
                score.risk      += 5 #medicos
                score.money     -= decimal.Decimal(8)
                score.influence += 0
                score.save()
                
            elif service == 'photo':
                respondent.took_photo = True
                score.risk      += 5 # pessoais
                score.money     -= decimal.Decimal(0)
                score.influence += 10
                score.save()
                
            elif service == 'twistter':
                respondent.installed_twistter = True;
                score.risk      += 10  # pessoais e relacion
                score.money     -= decimal.Decimal(0)
                score.influence += 10
                score.save()
                
            elif service == 'hospital-assist':
                respondent.installed_hospital_assist = True
                score.risk      += 5 # local
                score.money     -= decimal.Decimal(8.5)
                score.influence += 0
                score.save()
                
            respondent.save()
            
            messages.success(request, 'Operação realizada com sucesso!')
            
            return HttpResponseRedirect(reverse('history.views.step'))
        else:
            return HttpResponse(status = 400)
        
def add_utility_forms(curr_page='', data={}, respondent=None, request=None):
    
    if curr_page == '2c':
        personal_privacy_settings    = retrieve_settings(respondent, SETTINGS_PERSONAL)       
        financial_privacy_settings   = retrieve_settings(respondent, SETTINGS_FINANCIAL)
        location_privacy_settings    = retrieve_settings(respondent, SETTINGS_LOCATION)
        medical_privacy_settings     = retrieve_settings(respondent, SETTINGS_MEDICAL)
        relation_privacy_settings    = retrieve_settings(respondent, SETTINGS_RELATION)
        consumption_privacy_settings = retrieve_settings(respondent, SETTINGS_CONSUMPTION)
        
        data['personal_data_form']     = PersonalData(initial={'name':respondent.name, 'email':respondent.email, 'ipi':personal_privacy_settings.ipi, 'mode':personal_privacy_settings.mode, 'exposition':personal_privacy_settings.exposition})
        data['financial_data_form']    = FinancialData(initial={'ipi':financial_privacy_settings.ipi, 'mode':financial_privacy_settings.mode, 'exposition':financial_privacy_settings.exposition})
        data['location_data_form']     = LocationData(initial={'ipi':location_privacy_settings.ipi, 'mode':location_privacy_settings.mode, 'exposition':location_privacy_settings.exposition})
        data['medical_data_form']      = MedicalData(initial={'birth_date':respondent.birth_date, 'gender':respondent.gender, 'ipi':medical_privacy_settings.ipi, 'mode':medical_privacy_settings.mode, 'exposition':medical_privacy_settings.exposition})
        data['relationship_data_form'] = RelationshipData(initial={'ipi':relation_privacy_settings.ipi, 'mode':relation_privacy_settings.mode, 'exposition':relation_privacy_settings.exposition})
        data['consumption_data_form']  = ConsumptionData(initial={'ipi':consumption_privacy_settings.ipi, 'mode':consumption_privacy_settings.mode, 'exposition':consumption_privacy_settings.exposition})
        data['relationship_form']      = FriendForm()

    elif curr_page == '5a' or curr_page == '5b' or curr_page == '5c':
        request.session[NAV_SETTINGS_KEY] = SUPERMERCADO_TO_ENTRADA        
    elif curr_page == '8c':
        data['deduct_form'] = DeductForm(initial={'value': 0.0, 'context':'8c'})
    elif curr_page == '9b':
        request.session[NAV_SETTINGS_KEY] = CASA_TO_PAGARCONTA        
    elif curr_page == '9c' or curr_page == '9d':
        request.session[NAV_SETTINGS_KEY] = BURGUERKEEN_TO_CASA        
    elif curr_page == '13b':
        data['call_form'] = CallForm()
    elif curr_page == '15a':
        score = Score.objects.get(respondent = respondent)
        
        total = Score.objects.count()
        money_gt = Score.objects.filter(~Q(respondent = respondent),Q(money__lt=score.money)).count()
        
        influence_gt = 0
        if score.influence < 0: 
            influence_gt = Score.objects.filter(~Q(respondent = respondent),Q(influence__lt=0)).count()
        elif score.influence > 0:
            influence_gt = Score.objects.filter(~Q(respondent = respondent),Q(influence__gt=0)).count()
        else:
            influence_gt = Score.objects.filter(~Q(respondent = respondent),Q(influence=0)).count()
        risk_gt = Score.objects.filter(~Q(respondent = respondent),Q(risk=score.risk)).count()
        
        data['money_stat'] = (money_gt / total) * 100.
        data['influence_stat'] = (influence_gt / total) * 100.
        data['risk_stat'] = (risk_gt / total) * 100.
        data['normalized_score_influence'] = score.influence + 15 
    
    data['personal_exposure']    = calculate_exposure(respondent, SETTINGS_PERSONAL)
    #print 'personal_exposure ' + str(personal_exposure) 
    data['financial_exposure']   = calculate_exposure(respondent, SETTINGS_FINANCIAL)
    #print 'financial_exposure ' + str(financial_exposure) 
    data['location_exposure']    = calculate_exposure(respondent, SETTINGS_LOCATION)
    #print 'location_exposure ' + str(location_exposure) 
    data['medical_exposure']     = calculate_exposure(respondent, SETTINGS_MEDICAL)
    #print 'medical_exposure ' + str(medical_exposure) 
    data['relation_exposure']    = calculate_exposure(respondent, SETTINGS_RELATION)
    #print 'relation_exposure ' + str(relation_exposure) 
    data['consumption_exposure'] = calculate_exposure(respondent, SETTINGS_CONSUMPTION)
    #print 'consumption_exposure ' + str(consumption_exposure) 
    
    if respondent.installed_twistter:
        data['twistter_form'] = TwistterForm()
        
    if respondent.installed_fakebook:
        data['fakebook_form'] = FakebookForm(respondent)
    
    return data

def not_found(request):
    user       = request.user
    
    if user.is_authenticated:
        respondent = Respondent.objects.get(user = user) 
        user_step  = UserStep.objects.get(respondent = respondent)
        next_step  = user_step.step
        
        return render(request, 'history/error404.html', locals())  
    
    return HttpResponseRedirect('/login')

def is_repeated(respondent, step_page):
    step_query  = UserStep.objects.filter(respondent = respondent, step = step_page, answer__isnull=False)

    step = None
    if step_query:
        return True
    
    return False

def retrieve_step(respondent):
    step_query  = UserStep.objects.filter(respondent = respondent)
    
    step = None
    if step_query:
        step = step_query[0]
        
    if not step:
        step = UserStep()
        step.respondent = respondent
        step.step       = 'home'
        step.save()
        
    return step
        

def retrieve_settings(respondent, s_type):
    privacy_settings_query = PrivacySettings.objects.filter(respondent = respondent, type = s_type)
    
    privacy_settings = None
    if privacy_settings_query:
        privacy_settings = privacy_settings_query[0]
    
    if not privacy_settings:
        privacy_settings = PrivacySettings()
        privacy_settings.respondent = respondent
        privacy_settings.type = s_type
        privacy_settings.save()
        
    return privacy_settings

def calculate_exposure(respondent, data_type):
    if respondent is None:
        return 0.
    
    privacy_settings = retrieve_settings(respondent, data_type)
    
    time_diff = get_time_diff(respondent)
    
    diff_days     = time_diff / (60. * 60. * 24. * 30)
    diff_hours    = time_diff / (60. * 60. * 24.)
    diff_semester = time_diff / (60. * 60. * 24. * 30 * 6)
    
    if data_type == SETTINGS_PERSONAL:
        precision_name  = calculate_precision(0, 1)
        precision_email = calculate_precision(0, 1)
        exposure        = ((precision_name * privacy_settings.ipi) + (precision_email * privacy_settings.ipi)) / (100 * 2)
        
    elif data_type == SETTINGS_FINANCIAL:
        precision_account = calculate_precision(3.33, diff_days) # diário 100 / 30
        precision_card    = calculate_precision(0.1388888888888889, diff_hours) # hora
        exposure          = ((precision_account * privacy_settings.ipi) + (precision_card * privacy_settings.ipi)) / (100 * 2)
        
    elif data_type == SETTINGS_LOCATION:
        precision_location = calculate_precision(0.1388888888888889, diff_hours) #hora 100/(30 * 24)
        exposure           = (precision_location * privacy_settings.ipi) / (100 * 1)
        
    elif data_type == SETTINGS_RELATION:
        precision_relation = calculate_precision(3.33, diff_days)# diário 100 / 30
        exposure           = (precision_relation * privacy_settings.ipi) / (100 * 1)
        
    elif data_type == SETTINGS_MEDICAL:
        precision_medical = calculate_precision(0.5555555555555556, diff_semester) # semestre 100/(30 * 6)
        exposure          = (precision_medical * privacy_settings.ipi) / (100 * 1)
    elif data_type == SETTINGS_CONSUMPTION:
        precision_consumption = calculate_precision(3.33, diff_days) # diário 100 / 30
        exposure              = (precision_consumption * privacy_settings.ipi) / (100 * 1)
    else:
        exposure = 0.
    
    return exposure
    
def get_time_diff(respondent):
    user = respondent.user
    if user.date_joined:
        now = timezone.now()
        timediff = now - user.date_joined
        return timediff.total_seconds()

def calculate_precision(degradation, time):
    return 100 - (degradation * time)
    
@login_required
def call(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        call_form = CallForm(request.POST)
        
        if call_form.is_valid():
            data  = call_form.cleaned_data
    
            phone_number         = data['phone_number']
            respondent.called_to = phone_number
            respondent.save()
        
            return HttpResponse(status = 200)
        else:
            print call_form.errors
            messages.error(request, 'Erro ao completar sua ligação. Tem certeza que ligou para alguém?')

    return HttpResponse(status = 400)

@login_required
def mark_read(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        try:
            Message.objects.filter(receiver = respondent).update(read = True)
            return HttpResponse(status = 200)
        except:
            return HttpResponse(status = 400)

@login_required
def fakebook(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        fb_form = FakebookForm(respondent, request.POST)
                
        if fb_form.is_valid():
            data    = fb_form.cleaned_data
            
            content    = data['content']
            recever_id = data['receiver'] 
            receiver = Respondent.objects.get(pk = recever_id)
            
            if receiver: 
                message = Message()
                message.sender = respondent
                message.receiver = receiver
                message.content = content
                message.save()
                
                messages.success(request, 'Mensagem compartilhada com sucesso!')
                            
                return HttpResponse(status = 200)
            else:
                messages.error(request, 'Destinatário não encontrado')
        else:
            messages.error(request, 'Certifique-se de que você selecionou algum amigo e informou a mensagem a ser compatilhada!')
            messages.info(request, 'Adicione novos amigos ao seu perfil acessando, no menu à direita, o submenu \'Meus Dados\'')
            

    return HttpResponse(status = 400)

@login_required
def twistter(request):
    user       = request.user
    respondent = Respondent.objects.get(user = user)
    
    if request.method == 'POST':
        twistter_form = TwistterForm(request.POST)
        
        if twistter_form.is_valid():
            data  = twistter_form.cleaned_data
            text = data['text']
            
            respondent.used_twistter = True
            respondent.save()
            
            messages.info(request, '@voce: ' + str(text))
            
            return HttpResponse(status = 200)
        else:
            print twistter_form.errors
            messages.error(request, 'Erro ao enviar seu Twistte')

    return HttpResponse(status = 400)
