from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'history.views.home'),
    url(r'step/$', 'history.views.step'),
    url(r'profile/$', 'history.views.profile'),
    url(r'deduct/$', 'history.views.deduct_value'),
    url(r'service/$', 'history.views.install_service'),
    url(r'call/$', 'history.views.call'),
    url(r'twistter/$', 'history.views.twistter'),
    url(r'fakebook/$', 'history.views.fakebook'),
    url(r'fakebook/read/$', 'history.views.mark_read'),
    
)
