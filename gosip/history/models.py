# coding: utf-8
from django.db import models
from django.utils import timezone

from history.constants import INITIAL_AMOUNT_OF_MONEY
from identity.constants import SETTINGS_LOCATION, SETTINGS_PERSONAL, \
    SETTINGS_RELATION, SETTINGS_FINANCIAL, SETTINGS_MEDICAL
from identity.models import Respondent

class Message(models.Model):
    sender   = models.ForeignKey(Respondent, related_name='msg_sender')
    receiver = models.ForeignKey(Respondent, related_name='msg_receiver') 
    content  = models.CharField(max_length=300)
    read     = models.BooleanField(default=False)
    sent_at  = models.DateTimeField(default=timezone.now())
    
    class Meta:
        ordering = ['-sent_at','read']
        
# Create your models here.
class Score(models.Model):
    respondent= models.OneToOneField(Respondent, primary_key = True)
    influence = models.IntegerField(default=0)
    risk      = models.IntegerField(default=0)
    money     = models.DecimalField(default=INITIAL_AMOUNT_OF_MONEY, decimal_places=2, max_digits=9)
    
    def __str__(self):
        return '[I] ' + str(self.influence) + ', [R] ' + str(self.risk) + ' [D] ' + str(self.money)

class UserStep(models.Model):
    respondent = models.ForeignKey(Respondent)
    step       = models.CharField (max_length=50, default='home')
    answer     = models.CharField (max_length=50, null=True, blank=True)
    
    class Meta:
        ordering = ['-id']

class UserChoice(models.Model):
    respondent  = models.ForeignKey(Respondent)
    data_type   = models.CharField(max_length=200)
    context     = models.CharField(max_length=200)
    provider    = models.CharField(max_length=200)
    status      = models.CharField(max_length=200)
    modified    = models.DateTimeField(default=timezone.now())
    
    exposure_index =  models.DecimalField(default=0., decimal_places=2, max_digits=9)
        
class Step(object): 
    page    = '' # sem o .html
    goes_to = {}
    influence_impact = 0
    risk_impact = 0
    money_impact = 0.0
    
    def __init__(self, page='', goes_to={}, influence_impact=0, money_impact=0.0, risk_impact=0):
        self.page    = page
        self.goes_to = goes_to
        
        self.influence_impact = influence_impact
        self.risk_impact      = risk_impact
        self.money_impact     = money_impact
        
    def get_next(self, answer = ""):
        if (self.goes_to is None) or (answer not in self.goes_to):
            return None
        
        return self.goes_to[answer]
    
    def __str__(self):
        return '(' + self.page + ') :' + str(self.goes_to)

STEP = {}

STEP['home'] = Step('home', {'next_0':'0'})

STEP['0']  = Step('0', {'next_1':'1'})
STEP['1']  = Step('1', {'next_2b':'2b', 'next_2c':'2c'})

STEP['2a']  = Step('2a', {'next_3':'3'})
STEP['2b']  = Step('2b', {'next_15a':'15a'})
STEP['2c']  = Step('2c', {'next_2a':'2a'})

#STEP['3']  = Step('3', {'next_3a':'3a'})
STEP['3'] = Step('3', {'next_4a':'4a', 'next_4b':'4b', 'next_4c':'4c'})

STEP['4a'] = Step('4a', {'next_5a':'5a', 'next_5b':'5b', 'next_5c':'5c'})
STEP['4b'] = Step('4b', {'next_5d':'5d'})
STEP['4c'] = Step('4c', {'next_5e':'5e'}, 5, 0.0, 0) # tour com os amigos

STEP['5a'] = Step('5a', {'next_6a':'6a'})
STEP['5b'] = Step('5b', {'next_6a':'6a'})
STEP['5c'] = Step('5c', {'next_6a':'6a'})
STEP['5d'] = Step('5d', {'next_6b':'6b'})
STEP['5e'] = Step('5e', {'next_6c':'6c', 'next_6d':'6d', 'next_6e':'6e'})

STEP['6a']  = Step('6a', {'next_7a':'7a', 'next_7b':'7b'})
STEP['6b']  = Step('6b', {'next_7c':'7c'})
STEP['6c']  = Step('6c', {'next_7d':'7d'}, 5, 0, 15) #comprou o carro com pesquisa
STEP['6d']  = Step('6d', {'next_7d':'7d'}, 5, 0, 10) #comprou o carro sem pesquisa
STEP['6e']  = Step('6e', {'next_8c':'8c'})

STEP['7a'] = Step('7a', {'next_6b':'6b'})
STEP['7b'] = Step('7b', {'next_15a':'15a'})
STEP['7c'] = Step('7c', {'next_8a':'8a', 'next_8b':'8b'})
STEP['7d'] = Step('7d', {'next_8c':'8c'})

STEP['8a'] = Step('8a', {'next_9a':'9a', 'next_9b':'9b'})
STEP['8b'] = Step('8b', {'next_11a':'11a'}, -15, 0.0, 0) # não pagou a conta
STEP['8c'] = Step('8c', {'next_9c':'9c', 'next_9d':'9d'})

STEP['9a'] = Step('9a', {'next_10a':'10a'}, 0, 150.0, 5) #pagando conta de energia com cartão
STEP['9b'] = Step('9b', {'next_10b':'10b'}, 0, 150.0, 0) #pagando conta de energia com dinheiro
STEP['9c'] = Step('9c', {'next_10c':'10c'}, 0, 0, 5) #burqueen com cartão
STEP['9d'] = Step('9d', {'next_10c':'10c'}, 0, 0, 0) #burqueen com dinheiro

STEP['10a'] = Step('10a', {'next_11a':'11a'})
STEP['10b'] = Step('10b', {'next_10a':'10a', 'next_7c':'7c'})
STEP['10c'] = Step('10c', {'next_11b':'11b', 'next_11c':'11c', 'next_11d':'11d'})

STEP['11a'] = Step('11a', {'next_12a':'12a'})
STEP['11b'] = Step('11b', {'next_15a':'15a'})
STEP['11c'] = Step('11c', {'next_15a':'15a'})
STEP['11d'] = Step('11d', {'next_15a':'15a'})

STEP['12a'] = Step('12a', {'next_13a':'13a', 'next_13b':'13b', 'next_13c':'13c', 'next_14a':'14a', 'next_14b':'14b'})

STEP['13a'] = Step('13a', {'next_14a':'14a', 'next_14b':'14b'})
STEP['13b'] = Step('13b', {'next_14a':'14a'})
STEP['13c'] = Step('13c', {'next_15a':'15a'})

STEP['14a'] = Step('14a', {'next_15a':'15a'})
STEP['14b'] = Step('14b', {'next_15a':'15a'})

STEP['15a'] = Step('15a', {})

class Choice(object):
    data_type   = ''
    provider    = ''
    status      = ''

    def __init__(self, data_type="", provider="", status=""):
        self.data_type = data_type
        self.provider  = provider
        self.status    = status
        
#CHOICE['2a'] = [Choice(SETTINGS_PERSONAL, 'entrada em privus', 'governo', 'status'), Choice(SETTINGS_FINANCIAL, 'entrada em privus', 'governo', 'status')]

CHOICE = {}
CHOICE['home']  = []
CHOICE['0']  = [] 
CHOICE['1']  = [] 

CHOICE['2a']  = [] 
CHOICE['2b']  = [] 
CHOICE['2c']  = [] 

CHOICE['3']  = [] 

CHOICE['4a'] = [] 
CHOICE['4b'] = [] 
CHOICE['4c'] = [] 

CHOICE['5a'] = [Choice(SETTINGS_LOCATION, 'Gugou', 'status')] 
CHOICE['5b'] = [] 
CHOICE['5c'] = [] 
CHOICE['5d'] = [] 
CHOICE['5e'] = [] 

CHOICE['6a']  = [] 
CHOICE['6b']  = [] 
CHOICE['6c']  = [Choice(SETTINGS_PERSONAL, 'Concessionária de Autos', 'status'), Choice(SETTINGS_RELATION, 'Concessionária de Autos', 'status'), Choice(SETTINGS_FINANCIAL, 'Concessionária de Autos', 'status'), Choice(SETTINGS_MEDICAL, 'Concessionária de Autos', 'status')] 
CHOICE['6d']  = [Choice(SETTINGS_PERSONAL, 'Concessionária de Autos', 'status')] 
CHOICE['6e']  = [] 

CHOICE['7a'] = [] 
CHOICE['7b'] = [] 
CHOICE['7c'] = [] 
CHOICE['7d'] = [] 

CHOICE['8a'] = [] 
CHOICE['8b'] = [] 
CHOICE['8c'] = [Choice(SETTINGS_FINANCIAL, 'Pagamento com cartao', 'status')] 

CHOICE['9a'] = [] 
CHOICE['9b'] = [] 
CHOICE['9c'] = [] 
CHOICE['9d'] = [] 

CHOICE['10a'] = [] 
CHOICE['10b'] = [] 
CHOICE['10c'] = [] 

CHOICE['11a'] = [] 
CHOICE['11b'] = [] 
CHOICE['11c'] = [] 
CHOICE['11d'] = [] 

CHOICE['12a'] = [] 

CHOICE['13a'] = [] 
CHOICE['13b'] = [] 
CHOICE['13c'] = [] 

CHOICE['14a'] = [] 
CHOICE['14b'] = [] 

CHOICE['15a'] = [] 



