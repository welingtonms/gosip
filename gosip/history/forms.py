from django import forms

from identity.models import Respondent


class StepForm(forms.Form):
    step_page   = forms.CharField(widget=forms.HiddenInput(), max_length=100, min_length=1)
    step_answer = forms.CharField(widget=forms.HiddenInput(), max_length=100, min_length=1)
    
class DeductForm(forms.Form):
    value = forms.DecimalField(max_digits=9, decimal_places=2, widget=forms.HiddenInput())
    context = forms.CharField(widget=forms.HiddenInput(), max_length=100, min_length=1)
    
class ServiceForm(forms.Form):
    service  = forms.CharField(widget=forms.HiddenInput(), max_length=100, min_length=1)
    exposure = forms.CharField(widget=forms.HiddenInput(), max_length=100, min_length=1)
    
class CallForm(forms.Form):
    phone_number = forms.CharField( max_length=20, min_length=1)
    
class TwistterForm(forms.Form):
    text = forms.CharField(max_length=100, min_length=1)
    
class FakebookForm(forms.Form):
    receiver = forms.ChoiceField(widget=forms.Select(), required=True)
    content  = forms.CharField(max_length=400, min_length=1, required=True)
    
    def __init__(self, respondent, *args, **kwars):
        super(FakebookForm, self).__init__(*args, **kwars)
        self.fields['receiver'] = forms.ChoiceField(choices=[ (o.pk, str(o)) for o in respondent.friends.all()]) 